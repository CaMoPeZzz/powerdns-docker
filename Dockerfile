FROM alpine:3.14
LABEL org.opencontainers.image.authors="anton0baryshnikov@gmail.com"

ENV MYSQL_AUTOCONF=true \
    MYSQL_HOST="mysql" \
    MYSQL_PORT="3306" \
    MYSQL_USER="root" \
    MYSQL_PASS="root" \
    MYSQL_DB="pdns" \
    GUARDIAN_ENABLED="yes" \
    MYSQL_DNSSEC="no" \
    WEBSERVER_ENABLED="yes" \
    API_ENABLED="yes"

RUN apk --update add gettext pdns pdns-backend-mysql &&\
    addgroup -S appgroup && adduser -S appuser -G appgroup &&\
    mkdir -p /etc/pdns/conf.d/

ADD schema.sql pdns.conf.tpl /etc/pdns/
ADD entrypoint.sh /

EXPOSE 53/tcp 53/udp

ENTRYPOINT ["/entrypoint.sh"]
