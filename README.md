# PowerDNS Docker Container

Based on [psitrax/powerdns](https://github.com/psi-4ward/docker-powerdns)

* Small Alpine based Image
* MySQL (default) backend included
* DNSSEC support optional
* Automatic MySQL database initialization
* Latest PowerDNS version
* Guardian process enabled
* Graceful shutdown using pdns_control

## Usage

Example of docker-compose.yml

```shell
version: '3.3'

services:
  db:
    image: mysql:latest
    container_name: mysql
    command: --default-authentication-plugin=mysql_native_password
    restart: always
    environment:
      MYSQL_ROOT_PASSWORD: Passw0rd
    volumes:
      - ./mysql:/var/lib/mysql

  dns:
    image: powerdns:latest
    container_name: dns
    restart: always
    environment:
      MYSQL_USER: root
      MYSQL_DB: pdns
      MYSQL_PORT: 3306
      MYSQL_PASS: Passw0rd
      API_KEY: api-key
    ports:
      - "5053:53/tcp"
      - "5053:53/udp"
      - "8081:8081"
    depends
      - db
```

## Configuration

**Environment Configuration:**

* `MYSQL_AUTOCONF=true`
* `MYSQL_HOST="mysql"`
* `MYSQL_PORT="3306"`
* `MYSQL_USER="root"`
* `MYSQL_PASS="root"`
* `MYSQL_DB="pdns"`
* `GUARDIAN_ENABLED="yes"`
* `MYSQL_DNSSEC="no"`
* `WEBSERVER_ENABLED="yes"`
* `API_ENABLED="yes"`
* `API_KEY=`
* `ALLOWED_NETWORK=<Docker container network>`

**PowerDNS Configuration:**

Append the PowerDNS setting to the command as shown in the example above.
See `docker run --rm powerdns --help`


## License

[GNU General Public License v2.0](https://github.com/PowerDNS/pdns/blob/master/COPYING) applyies to PowerDNS and all files in this repository.


## Maintainer

* Anton Baryshnikov <anton0baryshnikov@gmail.com>

### Credits

* Christoph Wiechert <wio@psitrax.de>

